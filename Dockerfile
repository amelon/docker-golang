FROM golang:1.18.0-bullseye
MAINTAINER Vui Le "amelon@gmail.com"

RUN groupadd --gid 1000 dev \
 && useradd --uid 1000 --gid 1000 -m -s /bin/bash dev

#------------------------------------------------------------------------------
# Core tools
#------------------------------------------------------------------------------
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
      vim \
      curl \
      jq

#------------------------------------------------------------------------------
# Misc
#------------------------------------------------------------------------------
# Adjust timezone from UTC to PST
RUN mv /etc/localtime /etc/localtime.old \
    && ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

#------------------------------------------------------------------------------
# Go Tools
#------------------------------------------------------------------------------
ENV HOME /home/dev
USER dev
RUN mkdir -p /home/dev/tools /home/dev/workspace/bin /home/dev/workspace/pkg /home/dev/workspace/src
ENV GOPATH="/home/dev/workspace"
ENV PATH="${PATH}:/home/dev/tools:${GOPATH}/bin"
RUN go install golang.org/x/lint/golint@latest

COPY code/entrypoint_loop.sh /code/
COPY code/.bash_aliases /home/dev
WORKDIR /home/dev/workspace

