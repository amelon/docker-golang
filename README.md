# docker-golang
* https://bitbucket.org/amelon/docker-golang/

### Usage

Build the image:

    % docker-compose build

Start up the container:

    % docker-compose up -d

Check container status:

    % docker-compose ps

Get inside container with Bash shell:

    % docker-compose exec go bash

Shut down container:

    % docker-compose down

### Issues
* None

### History
* 2022-03-28 Updated to use Go v1.18.0.
* 2021-10-11 Updated to use Go v1.17.2.
